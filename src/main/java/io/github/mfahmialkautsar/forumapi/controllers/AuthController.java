package io.github.mfahmialkautsar.forumapi.controllers;

import io.github.mfahmialkautsar.forumapi.entities.dto.LoginUser;
import io.github.mfahmialkautsar.forumapi.entities.dto.RegisterUser;
import io.github.mfahmialkautsar.forumapi.entities.dto.RegisteredUser;
import io.github.mfahmialkautsar.forumapi.services.oauth.OAuth2UserDetailsService;
import io.github.mfahmialkautsar.forumapi.services.oauth.RolePathChecker;
import io.github.mfahmialkautsar.forumapi.usecases.IUserUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;


@RequestMapping("/v1")
@RestController
public class AuthController {
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private OAuth2UserDetailsService oauth2UserDetailsService;

    @Autowired
    private RolePathChecker rolePathChecker;

    @Autowired
    private IUserUseCase userUseCase;

    @Autowired
    private IUserUseCase iUserUseCase;

    @Value("${server.port}")
    private String PORT;

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/login")
    public ResponseEntity<Map> login(@RequestBody LoginUser loginUser) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + PORT)
                    .path("oauth/token")
                    .queryParam("username", loginUser.getUsername())
                    .queryParam("password", loginUser.getPassword())
                    .queryParam("grant_type", loginUser.getGrantType())
                    .queryParam("client_id", loginUser.getClientId())
                    .queryParam("client_secret", loginUser.getClientSecret());

            logger.info(builder.toUriString());

            return restTemplateBuilder.build().exchange(builder.toUriString(), HttpMethod.POST, null, new
                    ParameterizedTypeReference<Map>(){});
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, Object> map = new HashMap<>();

            map.put("status", 500);
            map.put("message", e);

            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/register")
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map<String, Object>> save(@Valid @RequestBody RegisterUser registerUser) {
        Map<String, Object> res = new HashMap<>();
        try {
            RegisteredUser user = userUseCase.register(registerUser);
            res.put("data", user);
            res.put("status", "success");
            return new ResponseEntity<>(res, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping("/oauth/authenticate")
    public ResponseEntity<Object> authenticateAction(
            @RequestParam Map<String, String> query,
            HttpServletResponse response,
            HttpServletRequest request,
            Principal principal) {
        Map<String, Object> res = new HashMap<>();
        try {
            String username = principal.getName();
            UserDetails user = null;

//            String xUri = request.getHeader("X-Uri");
//            if (!StringUtils.hasText(xUri) && query.containsKey("uri")) {
//                xUri = query.get("uri");
//            }

            if (StringUtils.hasText(username)) {
                user = oauth2UserDetailsService.loadUserByUsername(username);
            }

            if (null == user) {
                throw new UsernameNotFoundException("User not found");
            }

            if (!rolePathChecker.isAllow(user, request.getMethod())) {
                throw new Error("Not enough access to this endpoint");
            }

            response.addHeader("X-User", user.getUsername());

            Map<String, String> userFound = new HashMap<>();
            userFound.put("username", user.getUsername());

            res.put("status", "success");
            res.put("message", userFound);
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            res.put("status", "fail");
            res.put("message", e.getMessage());
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
    }
}
