package io.github.mfahmialkautsar.forumapi.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Table(name = "clients")
@Entity
@Getter
@Setter
public class Client implements ClientDetails, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String clientId;

    private String clientSecret;

    private String scope;

    private String grantTypes;

    private String redirectUri;

    private boolean isApproved;

    @Column(name = "access_token_validity_seconds")
    private Integer accessTokenValiditySeconds;

    @Column(name = "refresh_token_validity_seconds")
    private Integer refreshTokenValiditySeconds;

    @ManyToMany(targetEntity = Role.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "client_roles",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "roles_id"))
    private Set<GrantedAuthority> authorities = new HashSet<>();

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        Set<String> resources = new HashSet<>();
        resources.add("oauth2-resource");
        return resources;
    }

    @Override
    public boolean isSecretRequired() {
        return StringUtils.hasText(clientSecret);
    }

    @Override
    public boolean isScoped() {
        return StringUtils.hasText(scope);
    }

    @Override
    public Set<String> getScope() {
        Set<String> scopes = new HashSet<>();

        if (isScoped()) {
            scopes = new HashSet<>(Arrays.asList(scope.split("\\s")));
        }

        return scopes;
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        if (null != grantTypes) {
            return new HashSet<>(Arrays.asList(grantTypes.split("\\s")));
        }
        return null;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        if (null != redirectUri) {
            return new HashSet<>(Arrays.asList(redirectUri.split("\\s")));
        }
        return null;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String s) {
        return isApproved;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return new HashMap<>();
    }
}