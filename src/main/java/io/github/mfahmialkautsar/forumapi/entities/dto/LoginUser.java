package io.github.mfahmialkautsar.forumapi.entities.dto;

import lombok.Data;

@Data
public class LoginUser {
    private String username;
    private String password;
    private String grantType;
    private String clientId;
    private String clientSecret;
}
