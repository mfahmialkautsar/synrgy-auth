package io.github.mfahmialkautsar.forumapi.entities.dto;

import lombok.Data;

@Data
public class RegisterUser {
    private String username;
    private String fullName;
    private String password;
}
