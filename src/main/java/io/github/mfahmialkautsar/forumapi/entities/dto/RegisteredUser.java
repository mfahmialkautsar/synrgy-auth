package io.github.mfahmialkautsar.forumapi.entities.dto;

import lombok.Data;

@Data
public class RegisteredUser {
    public RegisteredUser(Long id, String username, String fullName) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
    }

    private Long id;
    private String username;
    private String fullName;
}
