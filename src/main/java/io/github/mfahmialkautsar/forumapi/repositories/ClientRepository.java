package io.github.mfahmialkautsar.forumapi.repositories;

import io.github.mfahmialkautsar.forumapi.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findOneByClientId(String clientId);
}