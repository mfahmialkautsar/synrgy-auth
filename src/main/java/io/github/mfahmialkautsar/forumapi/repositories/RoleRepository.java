package io.github.mfahmialkautsar.forumapi.repositories;

import io.github.mfahmialkautsar.forumapi.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findOneByName(String name);
    List<Role> findByNameIn(String[] names);
}