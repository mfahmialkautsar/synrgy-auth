package io.github.mfahmialkautsar.forumapi.repositories;

import io.github.mfahmialkautsar.forumapi.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsernameEquals(@NonNull String username);
}