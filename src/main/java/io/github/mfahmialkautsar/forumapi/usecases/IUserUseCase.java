package io.github.mfahmialkautsar.forumapi.usecases;

import io.github.mfahmialkautsar.forumapi.entities.dto.RegisterUser;
import io.github.mfahmialkautsar.forumapi.entities.User;
import io.github.mfahmialkautsar.forumapi.entities.dto.RegisteredUser;

public interface IUserUseCase {

    User getUserById(Long id);

    User getUserByUsername(String username);

    RegisteredUser register(RegisterUser registerUser) throws Exception;
}
