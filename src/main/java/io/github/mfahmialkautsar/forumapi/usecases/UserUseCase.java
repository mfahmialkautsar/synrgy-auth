package io.github.mfahmialkautsar.forumapi.usecases;

import io.github.mfahmialkautsar.forumapi.entities.Role;
import io.github.mfahmialkautsar.forumapi.entities.dto.RegisterUser;
import io.github.mfahmialkautsar.forumapi.entities.User;
import io.github.mfahmialkautsar.forumapi.entities.dto.RegisteredUser;
import io.github.mfahmialkautsar.forumapi.repositories.RoleRepository;
import io.github.mfahmialkautsar.forumapi.repositories.UserRepository;
import io.github.mfahmialkautsar.forumapi.utils.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserUseCase implements IUserUseCase {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserUseCase(RoleRepository roleRepository,
                       UserRepository userRepository,
                       PasswordEncoder passwordEncoder) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    public User getUserById(Long id) {
        return userRepository.getById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsernameEquals(username);
        if (!user.isPresent()) throw new Error(username + " not found");
        return user.get();
    }


    @Override
    public RegisteredUser register(RegisterUser registerUser) throws Exception {
        String[] roleNames = {"ROLE_USER", "ROLE_READ", "ROLE_WRITE"};
        String password = passwordEncoder.encode(registerUser.getPassword().replaceAll("\\s+", ""));
        List<Role> role = roleRepository.findByNameIn(roleNames);
        Optional<User> valUser = userRepository.findByUsernameEquals(registerUser.getUsername());
        if (valUser.isPresent()) throw new Exception("403");

        User user = UserMapper.registerUserToEntity(registerUser);
        user.setRoles(role);
        user.setPassword(password);

        User savedUser = userRepository.save(user);
        return UserMapper.entityToRegisteredUser(savedUser);
    }
}
