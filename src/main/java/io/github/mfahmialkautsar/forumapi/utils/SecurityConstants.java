package io.github.mfahmialkautsar.forumapi.utils;

import lombok.Data;

@Data
public class SecurityConstants {
//    public static final String SECRET = "SECK3yasd";
//    public static final long EXPIRATION_TIME = 900_000;
//    public static String TOKEN_PREFIX = "Bearer";
//    public static final String HEADER_STRING = "Authorization";
//    public static final String SIGN_UP_URL = "api/services/controller/user";

    String code = "status", message = "message";
    public String code_sukses = "200";
    public String code_server = "500";
    public String code_notFound = "404";
    public String code_null = "1";
    public String message_sukses = "sukses";
    public String message_alreadyexist = " already exist";
    public String notFoundUserID = "User id not found.";
    public String notFound = " not found.";
    public String isRequired = " is required";
    public String statusActive = "active";
    public String statusInActive = "inactive";
    public String statusPublish = "publish";
    public String statusNotPublish = "notpublished";
    public String statusDraft = "draft";
}
