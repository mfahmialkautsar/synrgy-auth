package io.github.mfahmialkautsar.forumapi.utils.mapper;

import io.github.mfahmialkautsar.forumapi.entities.dto.RegisterUser;
import io.github.mfahmialkautsar.forumapi.entities.dto.RegisteredUser;
import io.github.mfahmialkautsar.forumapi.entities.User;

public final class UserMapper {
    public static User registerUserToEntity(RegisterUser registerUser) {
        return new User(registerUser.getFullName(), registerUser.getUsername(), registerUser.getPassword());
    }

    public static RegisteredUser entityToRegisteredUser(User user) {
        return new RegisteredUser(user.getId(), user.getUsername(), user.getFullName());
    }

    public static RegisteredUser registerUserToRegisteredUser(RegisterUser registerUser) {
        User user = registerUserToEntity(registerUser);
        return new RegisteredUser(user.getId(), user.getUsername(), user.getFullName());
    }
}
